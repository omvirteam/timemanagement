<!DOCTYPE html>
<html>
  <head>
    <title>Project Management</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/app.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/main.css')?>">
  </head>
  <body>
    <div class="container">
      <br/>
      <br/>
      <?=anchor('welcome/logout', "Log out", array('class' => "pull-right"))?>
      <br/>
      <br/>
      <table class="table table-bordered">
        <tr>
          <th>#</th>
          <th>Project</th>
          <th>Task</th>
          <th>Start Date</th>
          <th>Target Date</th>
          <th>Target Minuts</th>
          <th>Worked Minuts</th>
        </tr>
        <?php $total = 0; foreach ($list as $i => $task) { ?>
        <tr>
          <td><?=($i+1)?></td>
          <td><?=$task->project?></td>
          <td><?=$task->note?></td>
          <td><?=$task->startDt?></td>
          <td><?=$task->targetDt?></td>
          <td><?=$task->targetMin?></td>
          <td><?=$task->workedMin?></td>
        </tr>
        <?php $total += $task->workedMin; } ?>
        <?php if($total > 0) { ?>
        <tr>
          <th colspan="6">Total</th>
          <th><?=$total?></th>
        </tr>
        <?php } ?>
      </table>
      <?=anchor('welcome/addEvent', "Add new")?>
    </div>
  </body>
</html>