<!DOCTYPE html>
<html>
  <head>
    <title>Project Management</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/app.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/main.css')?>">
  </head>
  <body>
    <div class="container">
      <br/>
      <br/>
      <div class="col-md-6">

          <?=form_open('welcome/saveEvent', array('class'=> "form-horizontal"))?>
          <div class="panel panel-default">
            <div class="panel-heading"><h3>Event Details</h3></div>
            <div class="panel-body" style="padding-left: 40px;padding-right: 40px;">
              <div class="form-group">
                <label for="project">Project</label>
                <select class="form-control" id="project" name="project">
                  <?php foreach ($projects as $project) { ?>
                    <option value="<?=$project->id?>"><?=$project->name?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="startDt">Start Date</label>
                <input type="date" class="form-control date" id="startDt" placeholder="Enter start date" name="startDt">
              </div>
              <div class="form-group">
                <label for="targetDt">Target Date</label>
                <input type="date" class="form-control date" id="targetDt" placeholder="Enter target date" name="targetDt">
              </div>
              <div class="form-group">
                <label for="targetMin">Target minutes</label>
                <input type="text" class="form-control" id="targetMin" placeholder="Enter target minutes" name="targetMin">
              </div>
              <div class="form-group">
                <label for="workedMin">Worked minutes</label>
                <input type="text" class="form-control" id="workedMin" placeholder="Enter worked minutes" name="workedMin">
              </div>
              <div class="form-group">
                <label for="note">Note</label>
                <textarea class="form-control" id="note" placeholder="Enter note" name="note"></textarea>
              </div>
            </div>
            <div class="panel-footer">
              <input class="btn btn-default" type="submit" value="Save"/>
              <?=anchor('welcome/report', "Back", array('class'=>"btn btn-default"))?>
            </div>
          </div>      
          <?=form_close()?>
      </div>
    </div>
  </body>
</html>