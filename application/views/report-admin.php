<!DOCTYPE html>
<html>
  <head>
    <title>Project Management</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/app.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/main.css')?>">
    <script type="text/javascript" src="<?=base_url('public/js/jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('public/js/bootstrap.min.js')?>"></script>
  </head>
  <body>
    <div class="container">
      <br/>
      <br/>
      <?=anchor('welcome/logout', "Log out", array('class' => "pull-right"))?>



      <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">all</a></li>
          <li role="presentation"><a href="#user" aria-controls="user" role="tab" data-toggle="tab">user</a></li>
          <li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">project</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="all">
            <br/>
            <br/>
            <table class="table table-bordered">
              <tr>
                <th>#</th>
                <th>User</th>
                <th>Project</th>
                <th>Task</th>
                <th>Start Date</th>
                <th>Target Date</th>
                <th>Target Minuts</th>
                <th>Worked Minuts</th>
              </tr>
              <?php $total = 0; foreach ($list as $i => $task) { ?>
              <tr>
                <td><?=($i+1)?></td>
                <td><?=$task->user?></td>
                <td><?=$task->project?></td>
                <td><?=$task->note?></td>
                <td><?=$task->startDt?></td>
                <td><?=$task->targetDt?></td>
                <td><?=$task->targetMin?></td>
                <td><?=$task->workedMin?></td>
              </tr>
              <?php $total += $task->workedMin; } ?>
              <?php if($total > 0) { ?>
              <tr>
                <th colspan="7">Total</th>
                <th><?=$total?></th>
              </tr>
              <?php } ?>
            </table>
          </div>
          <div role="tabpanel" class="tab-pane" id="user">
            <br/>
            <br/>
            <table class="table table-bordered">
              <tr>
                <th>#</th>
                <th>User</th>
                <th>Worked Minuts</th>
              </tr>
              <?php $total = 0; foreach ($users as $i => $user) { ?>
              <tr>
                <td><?=($i+1)?></td>
                <td><?=$user->name?></td>
                <td><?=$user->workDone==null?0:$user->workDone?></td>
              </tr>
              <?php $total += $user->workDone; } ?>
              <?php if($total > 0) { ?>
              <tr>
                <th colspan="2">Total</th>
                <th><?=$total?></th>
              </tr>
              <?php } ?>
            </table>
          </div>
          <div role="tabpanel" class="tab-pane" id="project">
            <br/>
            <br/>
            <table class="table table-bordered">
              <tr>
                <th>#</th>
                <th>User</th>
                <th>Worked Minuts</th>
              </tr>
              <?php $total = 0; foreach ($projects as $i => $project) { ?>
              <tr>
                <td><?=($i+1)?></td>
                <td><?=$project->name?></td>
                <td><?=$project->workDone==null?0:$project->workDone?></td>
              </tr>
              <?php $total += $project->workDone; } ?>
              <?php if($total > 0) { ?>
              <tr>
                <th colspan="2">Total</th>
                <th><?=$total?></th>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>

      </div>




      <?=anchor('welcome/addEvent', "Add new")?>
    </div>
  </body>
</html>