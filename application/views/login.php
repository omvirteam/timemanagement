<!DOCTYPE html>
<html>
  <head>
    <title>Project Management</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/app.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/main.css')?>">
  </head>
  <body>
    <div class="container">
      <br/>
      <br/>
      <div class="col-md-6">

          <?=form_open('welcome/login', array('class'=> "form-horizontal"))?>
          <div class="panel panel-default">
            <div class="panel-heading"><h3>Login</h3></div>
            <div class="panel-body" style="padding-left: 40px;padding-right: 40px;">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter username" name="username">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
              </div>
            </div>
            <div class="panel-footer"><input class="btn btn-default" type="submit" value="Login"/></div>
          </div>      
          <?=form_close()?>
      </div>
    </div>
  </body>
</html>