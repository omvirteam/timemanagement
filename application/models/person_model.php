<?php
class person_model extends CI_Model {

    // defining variables as we have column name in database table
    var $name       = '';
    var $username   = '';
    var $password   = '';
    var $type       = '';
    var $rate       = '';


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    

    /**
     *  login: this method is to attempt login
     *  arg:
     *     username: is username 
     *     password: is plain password (we are making it MD5 here it self)
     */
    function login($username, $password) {
        $query = $this->db->get_where('users', array(
            'username' => $username,
            'password' => md5($password)));
        return $query->row();
    }


    /*
    THIS ALL CODE IS NOT NEEDED AS OF NOW


    function get_all()
    {
        $query = $this->db
            ->select(array(
                "tasks.amt AS amt",
                "users.id AS id",
                "users.type AS type",
                "users.rate AS rate",
                "users.name AS name"))
            ->from('users')
            ->join('(SELECT user, SUM(rate*consume/60) AS amt FROM tasks GROUP BY user) AS tasks', "tasks.user = users.id")
            ->get();
        return $query->result();
    }

    function insert_entry()
    {
        $this->name   = $_POST['name']; // please read the below note
        $this->username   = $_POST['username']; // please read the below note
        $this->password   = md5($_POST['password']); // please read the below note
        $this->rate   = $_POST['rate']; // please read the below note
        $this->db->insert('users', $this);
    }

    function get_person($id) {
        $query = $this->db->get_where('users', array('id' => $id));
        return $query->row();
    }
    */

    // function update_entry()
    // {
    //     $this->title   = $_POST['title'];
    //     $this->content = $_POST['content'];
    //     $this->date    = time();

    //     $this->db->update('entries', $this, array('id' => $_POST['id']));
    // }

}
