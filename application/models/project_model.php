<?php
class project_model extends CI_Model {

    // defining variables as we have column name in database table
    var $name   = '';


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    /**
     *  get_all: method to select all projects
     */    
    function get_all()
    {
        $query = $this->db->get('projects');
        return $query->result();
    }

    /*
    THIS ALL CODE IS NOT REQUIRED AS OF NOW


    function insert_entry()
    {
        $this->name   = $_POST['name']; // please read the below note
        $this->db->insert('projects', $this);
    }

    function get_user_wise() {
        $query = $this->db
            ->select(array(
                "event.workDone AS workDone",
                "users.id AS id",
                // "users.type AS type",
                // "users.rate AS rate",
                "users.name AS name"))
            ->from('users')
            ->join('(SELECT user, SUM(workedMin) AS workDone FROM event GROUP BY user) AS event', "event.user = users.id", 'left')
            ->get();
        return $query->result();

    }

    function get_project($id) {
        $query = $this->db->get_where('projects', array('id' => $id));
        return $query->row();
    }

    // function update_entry()
    // {
    //     $this->title   = $_POST['title'];
    //     $this->content = $_POST['content'];
    //     $this->date    = time();

    //     $this->db->update('entries', $this, array('id' => $_POST['id']));
    // }
    */
}
