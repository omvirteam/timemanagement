<?php
class event_model extends CI_Model {

    // defining variables as we have column name in database table
    var $user = '';
    var $project = '';
    var $startDt = '';
    var $targetDt = '';
    var $targetMin = '';
    var $workedMin = '';
    var $note = '';


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    

    /**
     *  get_all: this method is to get all events along with associated project and persons
     */
    function get_all()
    {
        $query = $this->db
            ->select(array(
                'event.note AS note',
                'event.startDt AS startDt',
                'event.targetDt AS targetDt',
                'event.targetMin AS targetMin',
                'event.workedMin AS workedMin',
                'users.name AS user',
                'projects.name AS project'))
            ->from('event')
            ->join('projects', 'projects.id = event.project', 'left')
            ->join('users', 'users.id = event.user', 'left')
            ->get();
        return $query->result();
    }


    /**
     *  get_individual_all: this method is to get all events for particular user
     *  arg:
     *      uid: user id of the person for whom we want all events
     */
    function get_individual_all($uid)
    {
        $query = $this->db
            ->select(array(
                'event.note AS note',
                'event.startDt AS startDt',
                'event.targetDt AS targetDt',
                'event.targetMin AS targetMin',
                'event.workedMin AS workedMin',
                'projects.name AS project'))
            ->from('event')
            ->join('projects', 'projects.id = event.project', 'left')
            ->where(array('user'=> $uid))
            ->get();
        return $query->result();
    }


    /**
     *  load_user_event: this method is to get all events grouped by persons
     */
    function load_user_event() {
        $query = $this->db
            ->select(array(
                "event.workDone AS workDone",
                "users.id AS id",
                "users.name AS name"))
            ->from('users')
            ->join('(SELECT user, SUM(workedMin) AS workDone FROM event GROUP BY user) AS event', "event.user = users.id", 'left')
            ->get();
        return $query->result();
    }


    /**
     *  load_project_event: this method is to get all events grouped by project
     */
    function load_project_event() {
        $query = $this->db
            ->select(array(
                "event.workDone AS workDone",
                "projects.id AS id",
                "projects.name AS name"))
            ->from('projects')
            ->join('(SELECT project, SUM(workedMin) AS workDone FROM event GROUP BY project) AS event', "event.project = projects.id", 'left')
            ->get();
        return $query->result();
    }


    /**
     *  insert_entry: this method is to insert event
     *  arg:
     *      obj: is array object which contents key as column name and value as it's actual value
     */
    function insert_entry($obj)
    {
        foreach($obj as $key => $value) {
            $this->$key   = $value;
        }
        $this->db->insert('event', $this);
    }

}
