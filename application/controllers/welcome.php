<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{

		$user = $this->session->userdata('user');
		if($user != null) {

			// redirecting to dashboard (report page) page if user is already logged in
			redirect('welcome/report');
		}

		// getting error from session if it found
		$error = $this->session->userdata('error');
		if(isset($error)) {

			// removing from session once we get it
			$this->session->unset_userdata('error');
		}

		// prepare data to pass to login view
		$data = array('error' = $error);
		$this->load->view('login', $data);
	}


	/**
	 * login: this method is for user login 
	 *        Note: password is passing as plain text here,
	 * 							we are making it MD5 in model method
	 */
	public function login() {

		// loading person model and attempt login
		$this->load->model('person_model');
		$user = $this->person_model->login($_POST['username'], $_POST['password']);
		if($user == null) {

			// setting error message on unsuccessfull login attempt
			$this->session->set_userdata('error', "Invalid Credentials");
			redirect('welcome');

		} else {

			// setting user in session on successfull login 
			$this->session->set_userdata('user', $user);
			redirect('welcome/report');
		}
	}


	/**
	 * report: this method is to show user report as per user type (AMDIN or Other)
	 */
	public function report() {

		$user = $this->session->userdata('user');
		if($user == null) {

			// redirecting to welcome page if user is not logged in
			redirect('welcome');
		}

		// load event model
		$this->load->model('event_model');
		if($user->type == "ADMIN") {

			// admin report will display all user & all projects report
			$list = $this->event_model->get_all();
			$users = $this->event_model->load_user_event();
			$projects = $this->event_model->load_project_event();

			// creating data object to pass to view
			$data = array(
				'list' => $list,
				'users' => $users,
				'projects' => $projects);
			$this->load->view('report-admin', $data);

		} else {

			// normal user report will show only their own report
			$list = $this->event_model->get_individual_all($user->id);

			// creating data object to pass to view
			$data = array('list' => $list);
			$this->load->view('report', $data);
		}
	}


	/**
	 * addEvent: this method is to render event form
	 */
	public function addEvent() {

		// checking for loged in
		$user = $this->session->userdata('user');
		if($user == null) {

			// redirecting to welcome page if user is not logged in
			redirect('welcome');
		}

		// loading model and get all projects to show in project dropdown in event form
		$this->load->model('project_model');
		$projects = $this->project_model->get_all();

		// creating data object to pass to view
		$data = array('projects'=>$projects);
		$this->load->view('event-form', $data);
	}


	/**
	 * saveEvent: this method is to save event (event form supposed to be submitted here)
	 */
	public function saveEvent() {

		$user = $this->session->userdata('user');
		if($user == null) {

			// redirecting to welcome page if user is not logged in
			redirect('welcome');
		}

		// creating array object to be insert
		$event = array(
			'user'      => $user->id,
			'project'   => $_POST['project'],
			'startDt'   => $_POST['startDt'],
			'targetDt'  => $_POST['targetDt'],
			'targetMin' => $_POST['targetMin'],
			'workedMin' => $_POST['workedMin'],
			'note'      => $_POST['note']
		);

		// load model and do insertion
		$this->load->model('event_model');
		$this->event_model->insert_entry($event);

		// redirecting back to dashboard (report) page
		redirect('welcome/report');
	}


	/**
	 * logout: user logout
	 */
	public function logout() {

		// removing user variable from session
		$this->session->unset_userdata('user');

		// redirect back to home page
		redirect('welcome');
	}
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */